import { Col, Container, Row } from "react-bootstrap";
import SubTitle from "../components/SubTitle";
import { useSelector } from "react-redux";
import Resizable from "../components/Resizable";
import CardComponent from "../components/Card";

const Womens = () => {
  const productsList = useSelector((state) => state.productReducer.womensList);

  return (
    <Container>
      <Row>
        <Col>
          <Row className="mb-2">
            <SubTitle title="Women's Clothing" />
            <small className="mb-2">Total : {productsList.length}</small>
          </Row>
          <Row className=" mb-3">
            {productsList?.map((el, index) => (
              <Col key={index} className="mb-3">
                <Resizable>
                  <CardComponent
                    title={el.title.substring(0, 20)}
                    image={el.image}
                    description={el.description.substring(0, 75) + "..."}
                    price={el.price}
                    bgColor="#FF5E84"
                  />
                </Resizable>
              </Col>
            ))}
          </Row>
        </Col>
      </Row>
    </Container>
  );
};
export default Womens;
