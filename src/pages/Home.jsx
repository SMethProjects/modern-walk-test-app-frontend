import { Container, Row } from "react-bootstrap";
import FlashSale from "../components/FlashSale";
import Categories from "../components/Categories";

const Home = () => {
  return (
    <Container>
      <Row className="mb-2">
        <FlashSale />
      </Row>
      <Row>
        <Categories />
      </Row>
    </Container>
  );
};
export default Home;
