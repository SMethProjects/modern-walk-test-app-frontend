import { render, screen } from "@testing-library/react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import { Provider } from "react-redux";
import store from "../redux/store";
import Home from "../pages/Home";

const setUp = () => {
  const ReduxWrapper = ({ children }) => (
    <Provider store={store}>{children}</Provider>
  );
  const component = render(
    //browser router -fix error on '"useLocation,useNavigate Router component"""
    <BrowserRouter>
      <Home />
      <Routes>
        <Route path="/mens-clothing"></Route>
      </Routes>
    </BrowserRouter>,
    { wrapper: ReduxWrapper }
  );

  return component;
};
describe("Home page Component", () => {
  beforeEach(() => {
    setUp();
  });

  it("Should render  with text", () => {
    expect(screen.getByText(/Flash sale/i));
  });

  // it("Should render  with text", () => {
  //   expect(screen.getByText(/Categories/i));
  // });
  // it("Should render h3 heading with text", () => {
  //   expect(screen.getByRole("heading", { level: 3 })).toHaveTextContent(
  //     /Categories/i
  //   );
  // });

  // it("Should redirect to home page", () => {
  //   const linkButton = screen.getByText(/Modern Walk/i);
  //   fireEvent.click(linkButton, { button: 0 });
  //   expect(window.location.pathname).toBe("/");
  // });
});
