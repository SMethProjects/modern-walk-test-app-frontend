import { render, screen } from "@testing-library/react";
import { BrowserRouter, Routes } from "react-router-dom";
import { Provider } from "react-redux";
import store from "../redux/store";
import Women from "../pages/Women";

const setUp = () => {
  const ReduxWrapper = ({ children }) => (
    <Provider store={store}>{children}</Provider>
  );
  const component = render(
    //browser router -fix error on '"useLocation,useNavigate Router component"""
    <BrowserRouter>
      <Women />
      <Routes>{/* <Route path="/mens-clothing"></Route> */}</Routes>
    </BrowserRouter>,
    { wrapper: ReduxWrapper }
  );

  return component;
};
describe("Mens page Component", () => {
  beforeEach(() => {
    setUp();
  });

  it("Should render  with text", () => {
    expect(screen.getByText(/Women's CLothing/i));
  });
});
