import { fireEvent, render, screen } from "@testing-library/react";
import Navigation from "../components/Navigation";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import { Provider } from "react-redux";
import store from "../redux/store";

const setUp = () => {
  const ReduxWrapper = ({ children }) => (
    <Provider store={store}>{children}</Provider>
  );
  const component = render(
    //browser router -fix error on '"useLocation,useNavigate Router component"""
    <BrowserRouter>
      <Navigation />
      <Routes>
        <Route path="/"></Route>
      </Routes>
    </BrowserRouter>,
    { wrapper: ReduxWrapper }
  );

  return component;
};
describe("Navbar component", () => {
  beforeEach(() => {
    setUp();
  });

  it("Should render h1 heading with text", () => {
    expect(screen.getByRole("heading", { level: 1 })).toHaveTextContent(
      /Modern Walk/i
    );
  });

  it("Should redirect to home page", () => {
    const linkButton = screen.getByText(/Modern Walk/i);
    fireEvent.click(linkButton, { button: 0 });
    expect(window.location.pathname).toBe("/");
  });
});
