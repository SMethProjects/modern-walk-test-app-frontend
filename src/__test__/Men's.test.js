import { render, screen } from "@testing-library/react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import { Provider } from "react-redux";
import store from "../redux/store";
import Mens from "../pages/Mens";

const setUp = () => {
  const ReduxWrapper = ({ children }) => (
    <Provider store={store}>{children}</Provider>
  );
  const component = render(
    //browser router -fix error on '"useLocation,useNavigate Router component"""
    <BrowserRouter>
      <Mens />
      <Routes>
        <Route path="/mens-clothing"></Route>
      </Routes>
    </BrowserRouter>,
    { wrapper: ReduxWrapper }
  );

  return component;
};
describe("Mens page Component", () => {
  beforeEach(() => {
    setUp();
  });

  it("Should render  with text", () => {
    expect(screen.getByText(/Men's CLothing/i));
  });
});
