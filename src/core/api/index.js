import { api } from "../api";

// get All Categories
export const getCategories = () => {
  return api.get("/products/categories");
};

//get ALl Products
export const getAllProducts = () => {
  return api.get("/products");
};
