import axios from "axios";
const api = axios.create({
  ContentType: "application/json",
  baseURL: "https://fakestoreapi.com",
});

export { api };
