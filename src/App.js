import { Route, Routes } from "react-router-dom";
import SiteLayout from "./components/SiteLayout";
import Home from "./pages/Home";
import Mens from "./pages/Mens";
import Women from "./pages/Women";
import { Suspense } from "react";
import { Col } from "react-bootstrap";
import LoadingSpinner from "./components/LoadingSpinner";

function App() {
  return (
    <SiteLayout>
      <Suspense fallback={<Col className="">{<LoadingSpinner />}</Col>}>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/mens-clothing" element={<Mens />} />
          <Route path="/womens-clothing" element={<Women />} />
        </Routes>
      </Suspense>
    </SiteLayout>
  );
}

export default App;
