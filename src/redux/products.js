import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  mensList: [],
  womensList: [],
};

const productSlice = createSlice({
  name: "products",
  initialState: initialState,
  reducers: {
    mensList(state, action) {
      state.mensList = action.payload;
    },
    womensList(state, action) {
      state.womensList = action.payload;
    },
  },
});

export const productActions = productSlice.actions;
export default productSlice.reducer;
