import { Col, Container, Row } from "react-bootstrap";
import SubTitle from "./SubTitle";
import Category from "./Category";
import { getCategories } from "../core/api/index";
import { useEffect, useState } from "react";

const Categories = () => {
  const [categories, setCategories] = useState(null);
  const getAllCategories = async () => {
    await getCategories()
      .then((res) => {
        const filter = res.data.filter((it) => it.includes("clothing"));
        setCategories(filter);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  useEffect(() => {
    if (!categories) getAllCategories();
  }, [categories]);
  return (
    <Container>
      <Row>
        <Col>
          <Row className="mb-2">
            <SubTitle title="Categories" />
          </Row>
          <Row>
            {categories?.map((el, index) => (
              <Col key={index} md={6}>
                <Category
                  title={el}
                  bgColor={el == "men's clothing" ? "#2BD9AF" : "#FF5E84"}
                  link={
                    el == "men's clothing"
                      ? "/mens-clothing"
                      : "/womens-clothing"
                  }
                />
              </Col>
            ))}
          </Row>
        </Col>
      </Row>
    </Container>
  );
};
export default Categories;
