const SubTitle = ({ title }) => {
  return <h3>{title}</h3>;
};
export default SubTitle;
