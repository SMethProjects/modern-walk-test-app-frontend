import { Card } from "react-bootstrap";

const CardComponent = ({ title, image, description, price, bgColor }) => {
  return (
    <Card className="" style={{ borderRadius: "15%" }}>
      <Card.Body>
        <Card.Title>{title}</Card.Title>
        <Card.Img
          src={image}
          description={description}
          width="auto"
          height="120px"
        />
      </Card.Body>
      <div
        className="card-data"
        style={{
          borderRadius: "15%",
          backgroundColor: bgColor,
          padding: "10px",
        }}
      >
        <Card.Text style={{ color: "blue" }}>{price}</Card.Text>
        <Card.Text>{description} </Card.Text>
      </div>
    </Card>
  );
};
export default CardComponent;
