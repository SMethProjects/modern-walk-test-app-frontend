import { Col, Container, Row } from "react-bootstrap";
import SubTitle from "./SubTitle";
import CardComponent from "./Card";
import Resizable from "./Resizable";
import { useEffect, useState } from "react";
import { getAllProducts } from "../core/api/index";
import { useDispatch } from "react-redux";
import { productActions } from "../redux/products";

const FlashSale = () => {
  const [products, setProducts] = useState(null);
  const dispatch = useDispatch();
  const getProducts = async () => {
    await getAllProducts()
      .then((res) => {
        console.log(res.data);
        const filter = res.data.filter((it) =>
          it.category.includes("clothing")
        );
        const mens = res.data.filter((it) => it.category === "men's clothing");
        const women = res.data.filter(
          (it) => it.category === "women's clothing"
        );
        var ascending = filter.sort(
          (a, b) => Number(a.price) - Number(b.price)
        );
        dispatch(productActions.mensList(mens));
        dispatch(productActions.womensList(women));
        setProducts(ascending);
      })
      .catch((err) => {
        console.log(err);
      });
  };
  useEffect(() => {
    if (!products) getProducts();
  }, [products]);
  return (
    <Container>
      <Row>
        <Col>
          <Row className="mb-2">
            <SubTitle title="Flash Sale" />
          </Row>
          <Row className="scrollable-div mb-3">
            {products?.map((el, index) => (
              <Col key={index} className="mb-3">
                <Resizable>
                  <CardComponent
                    title={el.title.substring(0, 20)}
                    image={el.image}
                    description={el.description.substring(0, 75) + "..."}
                    price={"Rs " + el.price}
                    bgColor={
                      el.category == "men's clothing" ? "#2BD9AF" : "#FF5E84"
                    }
                  />
                </Resizable>
              </Col>
            ))}
          </Row>
        </Col>
      </Row>
    </Container>
  );
};
export default FlashSale;
