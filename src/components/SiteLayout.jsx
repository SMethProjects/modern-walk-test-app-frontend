import { Col, Container, Row } from "react-bootstrap";
import Navbar from "./Navigation";

const SiteLayout = ({ children }) => {
  return (
    <Container>
      <Row>
        <Col>
          <Navbar />
        </Col>
      </Row>
      <Row>
        <Col className=" " md={12}>
          <main>{children}</main>
        </Col>
      </Row>
    </Container>
  );
};

export default SiteLayout;
