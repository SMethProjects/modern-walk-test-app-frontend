import { Container, Navbar } from "react-bootstrap";
import { Link } from "react-router-dom";
// import { Link } from "react-router-dom";

const Navigation = () => {
  return (
    <Navbar className="bg-light">
      <Container className="justify-content-center">
        <Navbar.Brand className="">
          {/* <Link path=""></Link> */}

          <Link style={{ textDecoration: "none", color: "black" }} to="/">
            <h1>Modern Walk </h1>
          </Link>
          {/* </NavLink> */}
        </Navbar.Brand>
      </Container>
    </Navbar>
  );
};
export default Navigation;
