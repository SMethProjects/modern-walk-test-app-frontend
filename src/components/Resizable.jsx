// import { Card } from "react-bootstrap";
import { ResizableBox } from "react-resizable";

const Resizable = ({ children }) => {
  return (
    <div>
      <ResizableBox width={200} height="auto">
        <div style={{ backgroundColor: "", width: "100%", height: "100%" }}>
          {children}
        </div>
      </ResizableBox>
    </div>
  );
};
export default Resizable;
