// import { Button } from "react-bootstrap";

import { Col, Container, Row } from "react-bootstrap";
import { Link } from "react-router-dom";

const Category = ({ title, bgColor, link }) => {
  return (
    <Container>
      <Row>
        <Col>
          <button
            className="category border-0"
            style={{ backgroundColor: bgColor }}
          >
            <Link style={{ textDecoration: "none", color: "black" }} to={link}>
              <h3> {title}</h3>
            </Link>
          </button>
        </Col>
      </Row>
    </Container>
  );
};
export default Category;
